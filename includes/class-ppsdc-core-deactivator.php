<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://flauntyoursite.com
 * @since      1.0.0
 *
 * @package    Ppsdc_Core
 * @subpackage Ppsdc_Core/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ppsdc_Core
 * @subpackage Ppsdc_Core/includes
 * @author     William Bay <william@flauntyoursite.com>
 */
class Ppsdc_Core_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
